from flask import Flask, jsonify, request

app = Flask(__name__)

# Dati di esempio (puoi sostituire con i tuoi dati reali)
books = [
    {"id": 1, "title": "Il signore degli anelli", "author": "J.R.R. Tolkien"},
    {"id": 2, "title": "Harry Potter e la pietra filosofale", "author": "J.K. Rowling"},
    {"id": 3, "title": "1984", "author": "George Orwell"}
]

# Endpoint per ottenere tutti i libri
@app.route('/api/books', methods=['GET'])
def get_books():
    return jsonify(books)

# Endpoint per ottenere un libro specifico
@app.route('/api/books/<int:book_id>', methods=['GET'])
def get_book(book_id):
    book = next((book for book in books if book['id'] == book_id), None)
    if book:
        return jsonify(book)
    return jsonify({"message": "Libro non trovato"}), 404

# Endpoint per aggiungere un nuovo libro
@app.route('/api/books', methods=['POST'])
def add_book():
    data = request.json
    new_book = {
        "id": len(books) + 1,
        "title": data['title'],
        "author": data['author']
    }
    books.append(new_book)
    return jsonify(new_book), 201

if __name__ == '__main__':
    app.run(debug=True)
